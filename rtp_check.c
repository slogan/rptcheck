#include <sys/types.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <err.h>
#include <errno.h>
#include <net/if.h>
#include <netinet/in.h>
#include <net/if_arp.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/udp.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>


// RTP Stub structure
struct rtp{
	u_short hlam;
	u_short seq;
};


int main(int argc, char** argv) {
	int divertSock;
	struct sockaddr_in addr;
	struct sockaddr_in addr2;
	int addrSize2;
	int origBytes;
	int res=-1;
	unsigned char buf[2000];
	struct ip* iphdr;
	struct icmp* icmphdr;
	struct udphdr* udphdr;
	struct rtp* rtphdr;
	u_short oldRTPid;
	u_short newRTPid;
	u_short justStart;
	struct timeval tp, tpn;
	struct timezone tzp, tzpn;

	unsigned char Src[20];
	unsigned char Dst[20];

	unsigned char toDo[2000];
	int size_toDo;

	int logFile;

	if( argc != 3 ) {
		printf("Usage: %s DIVERT_PORT Log_FileName\n",argv[0]);
		exit(1);
	}

	divertSock = socket (PF_INET, SOCK_RAW, IPPROTO_DIVERT);
	if (divertSock == -1) {
		printf("Unable to create divert socket.");
		exit(2);
	}
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(atoi(argv[1]));

	res = bind (divertSock,(struct sockaddr*) &addr, sizeof(addr));
	if ( res == -1) {
		printf("Unable to bind divert socket.");
		exit(3);
	}

	// logFile = open(argv[2],O_RDWR|O_APPEND|O_CREAT,S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
	logFile = open(1);
	if( logFile == NULL ) {
		printf("Unable to open file %s for writing.",argv[2]);
		exit(4);
	}

	#define JUST_STARTED 1
	oldRTPid=0;
	justStart=JUST_STARTED;
	#define TH_DELTA 1
	while(1) {
		addrSize2  = sizeof( addr2 );
		origBytes = recvfrom ( divertSock, buf, sizeof(buf), 0,(struct sockaddr*) &addr2, &addrSize2 );
		iphdr=(struct ip*)buf;
		
		if( ( origBytes > (iphdr->ip_hl << 2) ) &&
			( iphdr->ip_p == 17 ) 
		) {
			udphdr=(struct udphdr*)(buf+(iphdr->ip_hl << 2));
			rtphdr=(struct rtp*)(buf+(iphdr->ip_hl << 2)+8);
			newRTPid=ntohs(rtphdr->seq);
			if( ( newRTPid != 0 ) && 
				( justStart != JUST_STARTED ) && 
				( (newRTPid-oldRTPid ) != TH_DELTA ) 
			){
				sprintf(Src,"%s",inet_ntoa(iphdr->ip_src));
				gettimeofday(&tp,&tzp);
				sprintf(Dst,"%s",inet_ntoa(iphdr->ip_dst));
				sprintf(toDo,"Time: %s", ctime(&tp.tv_sec));
				size_toDo=sprintf((toDo+30)," , Src: %s , Dst: %s , OldID: %d , NewID: %d\n",Src,Dst,oldRTPid,newRTPid);
				size_toDo=size_toDo+30;
				write(logFile,toDo,size_toDo);
			}
			justStart=!JUST_STARTED;
			oldRTPid=newRTPid;
		}
	}
	close(logFile);
	exit(0);
}

