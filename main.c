#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/udp.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <sys/stat.h>
#include <ctype.h>
#include <signal.h>


//use c++ only for std containers =) please forgive me...but raw c even without glib is a PAIN
#include <map>
#include <vector>
#include <string>
#include <fstream>

using std::vector;
using std::map;
using std::string;

struct MCtr;
typedef map<uint32_t,MCtr*> MCtrMap;
typedef map<uint32_t,MCtr*>::iterator StructsIter;
typedef vector<string> StringList;
typedef StringList::const_iterator StringListIter;


#define MAX_EVENTS 10
#define ERR -1
#define JUST_STARTED 1
#define TH_DELTA 1
#define MUSTBE_GREATER_THEN_ZERO 100
#define DONT_CHECK 0
#define DO_CHECK 1
#define ALIVECHANNEL_TO_MS 60
#define ALIVE_TH_US ALIVECHANNEL_TO_MS*1000
#define STREAM_ALIVE 0
#define STREAM_DEAD 404
#define DEFAULT_LISTEN_PORT 1234

int rc;
char local_ip[128];
char logs_dir[2048];
char config_file[4000];
int isStreamAliveCheckRequired=DONT_CHECK;
int fileOpenFlag=O_RDWR|O_APPEND|O_CREAT;


// RTP Stub structure
struct rtp{
	u_short hlam;
	u_short seq;
};




struct MCtr{
	int logFd;
	int errorFd;
	int justStarted;
	u_short rtpLastSeenId;
	string mcastGroup;
	long long int errorCount;
	u_short aliveCheckLastSeenId;
	int notifyState;

	MCtr(){
		logFd=-1;
		errorFd=-1;
		justStarted=JUST_STARTED;
		rtpLastSeenId=0;
		errorCount=0;
		notifyState=STREAM_ALIVE;
	}

};

MCtrMap groupStructs;


void printUsage(){
	printf(
		"\nBuild at "__DATE__" "__TIME__ " by: " WHO "@" WHERE " \n\n\n" \
		"Options: \n" \
		"\t-help this help \n" \
		"\t-i listen ip \n" \
		"\t-p listen port (default: 1234)\n" \
		"\t-l log directory (default: current dir)\n" \
		"\t-c config file full path (default: ./rtpcheck.conf)\n" \
		"\t-jk enable kolyan-jitter \n" \
		"\t-trunc trunc log files on start \n" \
		"\t e.g. -i 192.168.1.112 -p 1234 -l /tmp/mylogs_dir/ -c ./config.conf\n\n" \
		"\n\nConfig file example:\n" \
		"\t#comment line\n" \
		"\t239.64.65.87\n" \
		"\t239.64.65.100\n" \
		"\t#empty space ignored\n\n\n\n" \
		"\t239.64.65.1\n");
	exit(0);
}

void checkStreamAlive(MCtr* m){

	// printf("Group=%s m->justStarted=%u,m->notifyState=%u,m->aliveCheckLastSeenId=%u,m->rtpLastSeenId=%u\n",m->mcastGroup.c_str(),m->justStarted,m->notifyState,m->aliveCheckLastSeenId,m->rtpLastSeenId );
	//if there is no change between
	if ( m->aliveCheckLastSeenId == m->rtpLastSeenId && m->notifyState != STREAM_DEAD && m->justStarted != JUST_STARTED){
		char timebuf[128];
		m->notifyState=STREAM_DEAD;
		time_t t = time(NULL);
		struct tm * loctime = localtime(&t);
		strftime(timebuf, sizeof(timebuf), "%Y-%m-%d %H:%M:%S %Z", loctime);
		dprintf(m->logFd,"%s NOSTREAM Group %s LastSeenId %u \n",timebuf,m->mcastGroup.c_str(),m->aliveCheckLastSeenId);

	}else if( m->aliveCheckLastSeenId != m->rtpLastSeenId && m->notifyState == STREAM_DEAD && m->justStarted != JUST_STARTED){
		m->notifyState=STREAM_ALIVE;
	}

	//sync markers
	m->aliveCheckLastSeenId=m->rtpLastSeenId;
	return;
}

void checkStreamsAlive(){
	//iterate over groups
	StructsIter endIter=groupStructs.end();
	for( StructsIter iter=groupStructs.begin(),end=groupStructs.end(); iter!=end; iter++ ){
		checkStreamAlive(iter->second);
	}
}

void timePassedEvent(int signum){
	// char timebuf[128];
	// time_t t = time(NULL);
	if ( signum == SIGALRM ){
		// struct tm * loctime = localtime(&t);
		// strftime(timebuf, sizeof(timebuf), "%Y-%m-%d %H:%M:%S %Z", loctime);
		// printf("%s \n",timebuf);
		checkStreamsAlive();
		ualarm(ALIVECHANNEL_TO_MS*1000,0);
	}
}

int addMulticast(int targetSocket,char* mcast_group){
	struct ip_mreq group;

	//http://www.tldp.org/HOWTO/Multicast-HOWTO-6.html
	group.imr_multiaddr.s_addr = inet_addr(mcast_group);
	group.imr_interface.s_addr = inet_addr(local_ip);
	printf("Joining mcast group(%s) with socket(%u)\n",mcast_group,targetSocket);
	rc=setsockopt(targetSocket, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&group, sizeof(group));
	if( rc < 0 ){
		printf("FAILED: Multicast group(%s) join (code=%x str=%s)\n",mcast_group,rc,strerror(rc));
		close(targetSocket);
		return rc;
	}

	return 0;
}

int createLog(const char* mcast_group){
	char tmpbuf[4096]={0};
	sprintf(tmpbuf,"%s/%s.log",logs_dir,mcast_group);
	printf("Creating log file \"%s\"\n",tmpbuf);
	int fd=open(tmpbuf,fileOpenFlag);
	if(fd<=0){
			printf("FAILED: open file(\"%s\") for log (code=%x)\n",tmpbuf,fd);
	}
	chmod(tmpbuf,S_IRUSR|S_IRUSR|S_IRGRP|S_IWUSR|S_IROTH);
	return fd;
}

int createListener(int mcast_port ){
	//if we close 0,1,2 descriptions we can recieve created socket with 0 but...nvm =)
	int sd;
	int rc;
	int getFlag, setFlag;

	struct sockaddr_in localSock;

	sd = socket(AF_INET, SOCK_DGRAM, 0);

	if(sd < 0){
		printf("FAILED: UDP socket open (code=%x str=%s)\n",sd,strerror(sd));
		return sd;
	}

	int reuse = 1;
	rc=setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse, sizeof(reuse));
	if(rc<0){
		printf("FAILED: Setting SO_REUSEADDR\n");
		close(sd);
		return rc;
	}


	memset((char *) &localSock, 0, sizeof(localSock));
	localSock.sin_family = AF_INET;
	localSock.sin_port = htons(mcast_port);
	localSock.sin_addr.s_addr = INADDR_ANY;//inet_addr(local_ip);

	rc=bind(sd, (struct sockaddr*)&localSock, sizeof(localSock));
	if(rc<0) {
		printf("FAILED: UDP bind (code = %x)\n",rc);
		close(sd);
		return rc;
	}



	//setting to non blocking
	getFlag = fcntl(sd, F_GETFL, 0);
	if(getFlag == -1){
		printf("FAILED: fcntl get flag\n");
		return -1;
	}
	getFlag |= O_NONBLOCK;
	setFlag = fcntl(sd, F_GETFL, getFlag);

	if(setFlag == -1){
		printf("FAILED: fcntl set NONOBLOCK flag\n");
		return -1;
	}


	int opt=1;
	rc=setsockopt(sd, IPPROTO_IP, IP_PKTINFO, &opt, sizeof(opt));
	if(rc<0){
		printf("FAILED: Request IP info in UDP packet(code=%x)\n",rc);
		close(sd);
		return rc;
	}

	opt=1;
	rc=setsockopt(sd, IPPROTO_IP, IP_MULTICAST_LOOP, &opt, sizeof(opt));
	if(rc<0){
		printf("FAILED: Request IP_MULTICAST_LOOP (code=%x)\n",rc);
		close(sd);
		return rc;
	}


	return sd;
}

void dumpToHex(char* data,unsigned int len){
	for(unsigned int i=0;i<len;++i){
		printf("%02x ",data[i] & 0xff);
		if(i>0 && i%16==0){
			printf("\n");
		}
	}
	printf("\n");
	fflush(stdout);
}

int loop(MCtrMap* groups,int rootSocket){

	int datalen;
	char databuf[2000];
	struct timeval tp;
	struct timezone tzp;
	struct rtp* rtphdr;

	datalen=sizeof(databuf);

	// int epollFd = epoll_create(MUSTBE_GREATER_THEN_ZERO);

	// if(epollFd == -1){
	// 	printf("FAILED: epoll_create \n");
	// 	return ERR;
	// }

	// struct sockaddr_in senderAddr;
	// socklen_t senderAddrLen = sizeof(senderAddr);
	char cmbuf[0x128];
	char timebuf[128];
	struct sockaddr_in targetAddr;

	struct iovec iov[1];
	iov[0].iov_base = databuf;
	iov[0].iov_len = datalen;

	struct msghdr message;
	message.msg_name = &targetAddr;
	message.msg_namelen = sizeof(targetAddr);
	message.msg_iov=iov;
	message.msg_iovlen=1;
	message.msg_control = cmbuf;
	message.msg_controllen = sizeof(cmbuf);


	StructsIter endIter=groups->end();
	StructsIter findIter;
	unsigned int errCount=0;
	for(;;){
			//any performance decrease there possible?
			bzero(cmbuf,sizeof(cmbuf));
			size_t bytesRead=recvmsg(rootSocket, &message, 0);
			if( bytesRead > sizeof(rtphdr) ){
				u_short rtpNewId;
				uint32_t dstGroupIp;
				struct in_pktinfo *pi;
				MCtr *m;
				rtphdr=(struct rtp*)(databuf);
				rtpNewId=ntohs(rtphdr->seq);
				//we get multicast group as key for map
				for (struct cmsghdr *cmsg = CMSG_FIRSTHDR(&message);cmsg != NULL;cmsg = CMSG_NXTHDR(&message, cmsg)){
					// usually takes one hop to find,then we break to avoid unnec iterations
					if (cmsg->cmsg_level == IPPROTO_IP || cmsg->cmsg_type == IP_PKTINFO){
						pi = (struct in_pktinfo *)CMSG_DATA(cmsg);
						//http://lxr.free-electrons.com/ident?i=in_pktinfo
						dstGroupIp = *(uint32_t*)&(pi->ipi_addr);
						findIter = groups->find(dstGroupIp);
						if ( findIter != endIter ){
							m = findIter->second;

							// if( rtpNewId%1000 == 0 ){
							// 	printf("Group(%s) rtpNewId(%u) errCount(%d) dst(%s)\n",m->mcastGroup.c_str(),rtpNewId,errCount,inet_ntoa(pi->ipi_spec_dst));
							// }

							if( ( rtpNewId != 0 ) && 
								( m->justStarted != JUST_STARTED ) && 
								( (rtpNewId - m->rtpLastSeenId ) != TH_DELTA ) 
								){
									time_t t = time(NULL);
									struct tm * loctime = localtime(&t);
									//
									strftime(timebuf, sizeof(timebuf), "%Y-%m-%d %H:%M:%S %Z", loctime);
									errCount++;
									m->errorCount+=rtpNewId - m->rtpLastSeenId-1;
									dprintf(m->logFd,"%s Group %s  rtpNewId %u  lastId %u  errCount %d  dst %s errGroupCount %llu\n",timebuf,m->mcastGroup.c_str(),rtpNewId,m->rtpLastSeenId,errCount,inet_ntoa(pi->ipi_spec_dst),m->errorCount);
							}
							m->justStarted =! JUST_STARTED;
							m->rtpLastSeenId = rtpNewId;
							break;
						}
					}
				}//for opts

			}//if enough bytesRead
	}//for loop
	return 0;
}

int main(int argc, char *argv[]){
	StringList groups;
	int mcast_port=DEFAULT_LISTEN_PORT;
	int c;
	opterr = 0;
	#define DEFAULT_ERROR -10
	#define OKOK 0

	if(argc==1){
		printUsage();
	}

	strcpy(logs_dir,"./");
	strcpy(config_file,"./rtpcheck.conf");

	while ((c = getopt (argc, argv, "i:l:p:c:j:t:h:")) != -1){
		switch (c){
			case 'i':
				strcpy(local_ip,optarg);
				break;

			case 'p':
				mcast_port=atoi(optarg);
				break;

			case 'l':
				strcpy(logs_dir,optarg);
				break;

			case 'c':
				strcpy(config_file,optarg);
				break;

			case 't':
				printf("WARNING: -trunc option specified.Log files wille be truncated\n");
				fileOpenFlag|=O_TRUNC;
				break;

			case 'j':
				printf("WARNING: -jk option specified.kolyan-jitter enabled\n");
				isStreamAliveCheckRequired=DO_CHECK;
				break;

			case 'h':
				printUsage();
				break;//not reached

			default:
				printf("ERROR: Unknown option passed: \"%c\"\n",optopt);
				return DEFAULT_ERROR;

		}
	}

	printf("You passed:\n" \
		"\t config_file: %s\n"\
		"\t logs directory: %s\n"\
		"\t mcas_port: %u\n"\
		"\t local_ip: %s\n",config_file,logs_dir,mcast_port,local_ip);

	std::ifstream configStream(config_file);
	string group;
	for( ; getline( configStream, group ); ){
		if( group.size() <= 5 ){
			continue;
		}
		if( group[0]=='#'){
			continue;
		}
		groups.push_back(group);
	}

	for(StringListIter i = groups.begin(); i != groups.end(); ++i) {
		printf("\t mcast_group: %s\n",(*i).c_str());
	}

	if(groups.size()==0){
		printf("ERROR: No groups specified\n");
		return DEFAULT_ERROR;
	}

	printf("Total groups requested %u\n",groups.size());
	if( groups.size() > 20 ){
		printf("\nWARNING: You specified more then 20 multicast groups. Dont forget to set /proc/sys/net/ipv4/igmp_max_memberships value. Default is 20.\n\n");
	}

	int rootSocket=createListener(mcast_port);

	if(	rootSocket<=0){
		printf("FAILED: create socket\n");
		return -100500;
	}


	for(StringListIter i = groups.begin(); i != groups.end(); ++i) {
		int errorFd=0;
		int logFd=0;
		int rc=-1;
		struct in_addr tmpAddr;
		uint32_t key;//network order ipv4

		rc = addMulticast(rootSocket,(char *)(*i).c_str());
		if( rc < 0 ){
			printf("FAILED: addMulticast %s\n",(*i).c_str());
			return DEFAULT_ERROR;
		}
		MCtr* m=new MCtr();
		m->logFd=createLog((char *)(*i).c_str());
		if (m->logFd<=0){
			return DEFAULT_ERROR;
		}
		m->errorFd=errorFd;
		m->mcastGroup=string(*i);

		//use int representation of ip for faster comparision in map
		inet_aton((*i).c_str(),&tmpAddr);
		key=tmpAddr.s_addr;

		groupStructs[key]=m;
	}

	if ( isStreamAliveCheckRequired == DO_CHECK ){
		signal(SIGALRM, timePassedEvent);
		ualarm(ALIVECHANNEL_TO_MS*1000,0);
	}

	loop(&groupStructs,rootSocket);

	return OKOK;
}
