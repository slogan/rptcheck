#include <sys/types.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <net/if.h>
#include <net/route.h>
#include <netinet/in.h>
#include <netinet/ip_mroute.h>
#include <ifaddrs.h>
#include <stdlib.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <err.h>
#include <time.h>
#include <sys/time.h>

int main(int argc, char **argv){
	int fd, ret;
	int flag=1;
	struct ip_mreq stMreq;
	struct sockaddr_in stLclAddr;
	int n;
	unsigned char buff[1500];
	struct sockaddr_in server_addr;
	int server_addr_size=sizeof(server_addr);

	unsigned int ID;
	unsigned int OldID;
	struct timeval tp, tpn;
	struct timezone tzp, tzpn;


	if(argc!=2) {
		printf("Usage: sock 239.64.64.1\n");
		exit(2);
	}
	printf("Listening %s\n",argv[1]);
	fd=socket(AF_INET, SOCK_DGRAM, 0);
	if(fd<0) {
		printf("Socket  error\n");
		exit(1);
	}
	ret=setsockopt(fd,SOL_SOCKET,SO_REUSEADDR, &flag, sizeof(flag));
	if(ret!=0) {
		printf("SOL_SOCKET,SO_REUSEADDR error\n");
		exit(2);
	}
	
	stLclAddr.sin_family = AF_INET; 
	stLclAddr.sin_addr.s_addr = htonl(INADDR_ANY); 
	stLclAddr.sin_port = htons(1234); 
	ret = bind(fd,(struct sockaddr*) &stLclAddr,sizeof(stLclAddr)); 
	if(ret!=0) {
		printf("Bind error\n");
		exit(3);
	}

	
	stMreq.imr_multiaddr.s_addr = inet_addr(argv[1]); 
	stMreq.imr_interface.s_addr = INADDR_ANY;  
	ret= setsockopt(fd,IPPROTO_IP,IP_ADD_MEMBERSHIP,(char *)&stMreq,sizeof(stMreq));
	if(ret!=0) {
		printf("IPPROTO_IP,IP_ADD_MEMBERSHIP error\n");
		exit(4);
	}


	OldID=0;
	buff[2]=0;
	buff[3]=0;

	while(1)
	{
		OldID=(buff[2])*256+ buff[3];
		n=recvfrom(fd,&buff[0],sizeof(buff)-1,0, (struct sockaddr *) &server_addr, &server_addr_size);
		ID=(buff[2])*256+ buff[3];
		if((ID-OldID>1)&&(ID!=0)) {
			gettimeofday(&tp,&tzp);
			printf("OldID: %d , ID:%d, Time: %s",OldID, ID, ctime(&tp.tv_sec));
		}

	}


}
