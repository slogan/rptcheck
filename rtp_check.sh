bin="/usr/local/comcom/monitoring/rtp_check"



case "$1" in
    start)
    $bin 8001 >> /var/log/mcast1.log &
    $bin 8002 >> /var/log/mcast2.log &
    $bin 8003 >> /var/log/mcast3.log &
    $bin 8004 >> /var/log/mcast4.log &
    $bin 8005 >> /var/log/mcast5.log &
    $bin 8006 >> /var/log/mcast6.log &
    $bin 8007 >> /var/log/mcast7.log &
    $bin 8008 >> /var/log/mcast8.log &
    $bin 8009 >> /var/log/mcast9.log &
    $bin 8010 >> /var/log/mcast10.log &
    $bin 8011 >> /var/log/mcast11.log &
    $bin 8012 >> /var/log/mcast12.log &
    $bin 8013 >> /var/log/mcast13.log &
    $bin 8014 >> /var/log/mcast14.log &
    $bin 8015 >> /var/log/mcast15.log &
    $bin 8016 >> /var/log/mcast16.log &
    $bin 8017 >> /var/log/mcast17.log &
    $bin 8018 >> /var/log/mcast18.log &
    $bin 8019 >> /var/log/mcast19.log &
    $bin 8020 >> /var/log/mcast20.log &
        ;;
    stop)
        echo "Killing $bin"
        killall rtp_check
        ;;
    *)

        echo "Usage:  rtp_check {start | stop}"
        exit 1
esac

exit $RETVAL

