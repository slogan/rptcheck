CC = g++
CFLAGS = -g -O3 -Wextra -Werror -DWHO=\"`whoami`\" -DWHERE=\"`uname -r`\"
OBJECTS = main.o

all : $(OBJECTS)
		$(CC) $(CFLAGS) $(OBJECTS) -o rtp_check.bin

%.o : %.c
		$(CC) $(CFLAGS) -c $<
